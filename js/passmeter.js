$(function() {
	let classesComplexidade = 'label-danger label-success label-default label-primary label-info label-warning';
	let labelClass = { 'Muito fraca': 'label-danger', 'Fraca': 'label-danger', 'Boa': 'label-warning', 'Forte': 'label-success', 'Muito forte': 'label-success'};

	$('document').ready(function() {		
		$('.div-senha input').keyup(senhaKeyUp);
		$('.botao-open').click(botaoOpenClick);		
		$('.botao-close').click(botaoCloseClick);	
		inicializar();	
	});	

	function botaoOpenClick() {
		$('#tb-descricao-score').show();
		$('.botao-close').show();
		$('.botao-open').hide();
	};

	function botaoCloseClick() {
		$('#tb-descricao-score').hide();
		$('.botao-close').hide();
		$('.botao-open').show();
	};

	function inicializar() {
		$('.div-senha input').val('');
		$('.div-erro').hide();
		$('#tb-descricao-score').hide();
		$('.botao-close').click();
		$('.div-senha .percentual').text(`0 %`);
		$('.div-senha .complexidade')
			.removeClass(classesComplexidade)
			.addClass('label-danger')
			.text('Muito curta');
	};

	function senhaKeyUp() {
		if ($(this).val().length == 0) { 
			inicializar();
			return; 
		}
		$.ajax({
			url: `http://localhost:8080/meter/${$(this).val()}`,
			method: 'GET'
		}).done(function(result) {
			$('.div-senha .percentual').text(`${result.score} %`);
			$('.div-senha .complexidade')
										.removeClass(classesComplexidade)
										.addClass(labelClass[result.resultado])
										.text(`${result.resultado}`);
			atualizarTabela(result.itens) 
		}).fail(function() {
			$('.div-erro').show();
		});
	};

	function atualizarTabela(itens) {
		let table = document.getElementById("tb-descricao-score");
		table.innerHTML = "";
		let i = 0;
		itens.forEach(item => {			
		    let row = table.insertRow(i);
		    let cell1 = row.insertCell(0);
		    let cell2 = row.insertCell(1);
		    let cell3 = row.insertCell(2);
		    cell1.innerHTML = item.descricao;
		    cell2.innerHTML = item.quantidade;
		    cell3.innerHTML = item.bonus;
		    i++;
		});
	};
});

